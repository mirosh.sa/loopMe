(function init(plugins) {
    var CONSTANTS = {
        mainContainer: plugins.getElem('main_container'),
        playerHolder: plugins.getElem('player_holder'),
        teaserImage: plugins.getElem('teaser_image'),
        lastFrame: plugins.getElem('teaser_last_frame'),
        hints: plugins.getElem('hints_holder').children,
        sliderElem: plugins.getElem('dragbar'),
        thumbElem: plugins.getElem('dragbar_control'),
        dropsHolder: plugins.getElem('drops_holder'),
        closeHolder: plugins.getElem('close_holder'),
        countMainTeaserSlider: 58,
        maxCountDrops: 70,
        advertPageUrl: '',
        player: null
    };

    CONSTANTS.closeHolder.addEventListener('click', function () {
        CONSTANTS.mainContainer.parentNode.removeChild(CONSTANTS.mainContainer);
    }, false);

    var preventClick = false;
    document.querySelector('.content_holder').addEventListener('click', function (e) {
        if (!preventClick) {
            var win = window.open(CONSTANTS.advertPageUrl, '_blank');
            win.focus();
        }
    }, false);


    var handleSliderChange = function (currentSlide) {
        plugins.rotateMainTeaser(currentSlide, CONSTANTS.teaserImage, CONSTANTS.lastFrame, CONSTANTS.countMainTeaserSlider);
        plugins.rotateHints(currentSlide, CONSTANTS.hints);
        plugins.controlYTPlayer(currentSlide, CONSTANTS.player, CONSTANTS.playerHolder);
        plugins.showDrops(currentSlide, CONSTANTS.maxCountDrops, CONSTANTS.dropsHolder);
    };

    (function setSliderControl() {

        CONSTANTS.thumbElem.onmousedown = function (e) {
            e.stopPropagation();
            var shiftY = e.pageY - plugins.getCoords(CONSTANTS.thumbElem);
            preventClick = true;

            document.onmousemove = function (e) {
                var newTop = e.pageY - shiftY - plugins.getCoords(CONSTANTS.sliderElem);
                if (newTop < 0) {
                    newTop = 0;
                }
                var bottomEdge = CONSTANTS.sliderElem.offsetHeight - CONSTANTS.thumbElem.offsetHeight;
                if (newTop > bottomEdge) {
                    newTop = bottomEdge;
                }
                var slidePosition = parseInt((newTop / bottomEdge) * CONSTANTS.countMainTeaserSlider, 10);
                handleSliderChange(slidePosition);
                CONSTANTS.thumbElem.style.top = newTop + 'px';
            };

            document.onmouseup = function (e) {
                e.stopPropagation();
                document.onmousemove = document.onmouseup = null;
                setTimeout(function () {
                    preventClick = false;
                }, 0);

            };

        };
        CONSTANTS.thumbElem.ondragstart = function () {
            return false;
        };
    })();

    (function appendYTPlayer() {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var onPlayerReady = function onPlayerReady(event) {
            CONSTANTS.player.mute();
            CONSTANTS.player.playVideo();
        };

        (function waitYTData() {
            if (window.YT && window.YT.Player) {
                CONSTANTS.player = new YT.Player('ytplayer', {
                    height: '202',
                    width: '363',
                    videoId: '9xKR8Vcjias',
                    playerVars: {
                        autoplay: 1,
                        showinfo: 0,
                        fs: 0
                    },
                    events: {
                        'onReady': onPlayerReady
                    }
                });

                return;
            }
            setTimeout(waitYTData, 40);
        }());
    })();

})({
    getCoords: function (elem) {
        return elem.getBoundingClientRect().top + pageYOffset;
    },
    getElem: function (id) {
        return document.getElementById(id);
    },
    rotateMainTeaser: function (slide, teaserFrame, lastFrame, countSlides) {
        var srcMainPath = 'img/phone/phone_';
        var imgPath = '000' + (parseInt(slide, 10) < 10 ? '0' : '') + slide;
        teaserFrame.src = srcMainPath + imgPath + '.png';
        this.animateOpacity(lastFrame, slide >= countSlides);
    },
    rotateHints: function (slide, hintsElems) {
        var _this = this;
        var showHint = 3;
        if (slide < 3) {
            showHint = 0;
        } else if (slide < 23) {
            showHint = 1;
        } else if (slide < 47) {
            showHint = 2;
        }
        Array.prototype.slice.call(hintsElems).map(function (hint, index) {
            _this.animateOpacity(hint, showHint === index);
        });

    },
    controlYTPlayer: function (slide, player, playerHolder) {
        if (player) {
            var playerStyle = 'block';
            if (slide > 0) {
                player.pauseVideo();
                playerStyle = 'none';
            } else {
                player.playVideo();
            }
            playerHolder.style.display = playerStyle;
        }

    },
    makeRandom: function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    },
    createDrop: function (dropsContainer) {
        var _this = this;
        var delay = this.makeRandom(10, 5000);
        var drop = document.createElement('div');
        var dropType = this.makeRandom(1, 5);
        drop.className = 'drop drop-type' + dropType;
        drop.style.top = this.makeRandom(1, 100) + '%';
        drop.style.left = this.makeRandom(1, 93) + '%';

        setTimeout(function () {
            if (dropsContainer) {
                dropsContainer.appendChild(drop);
                _this.doAnimationDrop(drop, dropsContainer);
            }
        }, delay);

    },
    doAnimationDrop: function (drop, dropsContainer) {
        var delayZoom = this.makeRandom(0, 100);
        setTimeout(function () {
            drop.classList.add('animating-zoom');
        }, delayZoom);

        var delayDown = this.makeRandom(delayZoom, delayZoom * 10);
        setTimeout(function () {
            drop.classList.add('animating-down');
            var dropsHolder = dropsContainer.parentNode;
            if (dropsHolder) {
                var animatedDrops = (parseInt(dropsHolder.getAttribute('animated-drops'), 10) || 0) - 1;
                dropsHolder.setAttribute('animated-drops', animatedDrops.toString());
            }
        }, delayDown);
    },
    showDrops: function (slide, maxCountDrops, dropsHolder) {

        var dropsSlideRange = (slide > 0 && slide < 23);
        if (dropsHolder.getAttribute('animated-drops') === '0' && dropsSlideRange) {
            dropsHolder.innerHTML = '';
            dropsHolder.setAttribute('animated-drops', maxCountDrops);

            var dropsContainer = document.createElement('div');
            dropsHolder.appendChild(dropsContainer);

            for (var i = 0; i < maxCountDrops; i++) {
                this.createDrop(dropsContainer);
            }
        } else if (!dropsSlideRange) {
            dropsHolder.innerHTML = '';
            dropsHolder.setAttribute('animated-drops', '0');
        }


    },
    animateOpacity: function (elem, isShown) {
        elem.style.opacity = isShown ? 1 : 0;
        elem.style.zIndex = isShown ? 1000 : -1;
    }
});